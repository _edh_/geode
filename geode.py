#! /usr/bin/python3
# coding:utf-8

import math
import sys

class SphericalPoint:
    def __init__(self, x, y, z):
        d = math.hypot(x, y, z)
        self.x = x / d
        self.y = y / d
        self.z = z / d
    def __str__(self):
        return f"Point({self.x:+.2f}, {self.y:+.2f}, {self.z:+.2f})"

class Vertex:
    def __init__(self, *, point=None, weights=None, vertices=None):
        if point is not None:
            self.point = point
            self.ident = self.point
        else:
            self.point = None
            weightedvertices = [(weight,vertex) for weight,vertex in zip(weights, vertices) if weight!=0]
            if len(weightedvertices) == 1:
                self.point = weightedvertices[0][1].point
                self.ident = self.point
            elif len(weightedvertices) == 2:
                if weightedvertices[0] > weightedvertices[1]:
                    weightedvertices = weightedvertices[1:] + [weightedvertices[0]]
                self.weights,self.vertices = zip(*weightedvertices)
                self.ident = (self.weights, self.vertices)
            elif len(weightedvertices) == 3:
                if weightedvertices[0] > weightedvertices[1]:
                    weightedvertices = weightedvertices[1:] + [weightedvertices[0]]
                    if weightedvertices[0] > weightedvertices[1]:
                        weightedvertices = weightedvertices[1:] + [weightedvertices[0]]
                elif weightedvertices[0] >= weightedvertices[2]:
                        weightedvertices = [weightedvertices[-1]] + weightedvertices[:-1]
                self.weights,self.vertices = zip(*weightedvertices)
                self.ident = (self.weights, self.vertices)
            else:
                assert(False)

    def __gt__(self, other):
        return id(self) > id(other)
    def __ge__(self, other):
        return id(self) >= id(other)

    def __str__(self):
        if self.point:
            return str(self.point)
        return "Vertex(" + ", ".join([f"{vertex}*{weight}" for weight,vertex in zip(self.weights, self.vertices)]) + ")"
    __repr__ = __str__

    def project(self):
        if self.point:
            return
        xs,ys,zs = zip(*[(vertex.point.x,vertex.point.y,vertex.point.z) for vertex in self.vertices])
        x = sum([weight * x for weight,x in zip(self.weights,xs)])
        y = sum([weight * y for weight,y in zip(self.weights,ys)])
        z = sum([weight * z for weight,z in zip(self.weights,zs)])
        self.point = SphericalPoint(x,y,z)

class HalfEdge:
    def __init__(self, vertex):
        self.vertex = vertex
        self.face = None
        self.next = None
        self.opposite = None

    @staticmethod
    def fusion(edges):
        edgesbyvertices = {}
        for edge in edges:
            key = frozenset((edge.vertex,edge.next.vertex))
            if key in edgesbyvertices:
                opposite = edgesbyvertices.pop(key)
                edge.opposite = opposite
                opposite.opposite = edge
            else:
                edgesbyvertices[key] = edge

    @staticmethod
    def lengths(edges):
        edgesbyvertices = {}
        lengths = []
        for edge in edges:
            key = frozenset((edge.vertex,edge.next.vertex))
            if key in edgesbyvertices:
                edgesbyvertices.pop(key)
            else:
                edgesbyvertices[key] = edge
                P = edge.vertex.point
                Q = edge.next.vertex.point
                l = math.hypot(P.x-Q.x, P.y-Q.y, P.z-Q.z)
                lengths.append(l)
        return sorted(lengths)

class Face:
    def __init__(self, *edges):
        self.oneedge = edges[0]
        for i,edge in enumerate(edges):
            edges[i-1].next = edge
            edge.face = self

class Geode:
    tetrahedron = dict(
        points=((1,1,1),(1,-1,-1),(-1,1,-1),(-1,-1,1)),
        faces=((0,1,2),(0,2,3),(0,3,1),(3,2,1))
    )
    octahedron = dict(
        points=((1,0,0),(0,1,0),(0,0,1),(-1,0,0),(0,-1,0),(0,0,-1)),
        faces=((0,1,2),(2,1,3),(2,3,4),(2,4,0),(0,5,1),(1,5,3),(3,5,4),(4,5,0))
    )
    phi = (1 + math.sqrt(5)) / 2
    icosahedron = dict(
        points=((+phi,+1,0),(-phi,+1,0),(-phi,-1,0),(+phi,-1,0),
                (0,+phi,+1),(0,-phi,+1),(0,-phi,-1),(0,+phi,-1),
                (+1,0,+phi),(+1,0,-phi),(-1,0,-phi),(-1,0,+phi)),
        faces=((0,7,4),(0,4,8),(0,8,3),(0,3,9),(0,9,7),
                   (1,2,11),(1,10,2),(1,7,10),(1,4,7),(1,11,4),
                   (4,11,8),
                   (5,3,8),(5,8,11),(5,11,2),(5,2,6),(5,6,3),
                   (6,9,3),(6,10,9),(6,2,10),
                   (7,9,10))
    )
    nucleusdict = dict(III=tetrahedron, IV=octahedron, V=icosahedron)
    def __init__(self, N, m, n):
        if not N in Geode.nucleusdict:
            raise ValueError(f"parameter 'N' must be one of {tuple(Geode.nucleusdict.keys())})")
        self.N = N
        if not isinstance(m, int):
            raise TypeError(f"parameter 'm' must be an int not a {type(m).__name__}")
        if not isinstance(n, int):
            raise TypeError(f"parameter 'n' must be an int not a {type(n).__name__}")
        if m < 1:
            raise ValueError(f"parameter 'm' must be strictly positive (got {m})")
        if n < 0:
            raise ValueError(f"parameter 'n' must be positive (got {n})")
        if n > m:
            raise ValueError(f"parameter 'n' must be smaller than 'm' (got {n} and {m})")
        self.m = m
        self.n = n

        nucleus = Geode.nucleusdict[N]
        self.nucleusvertices = [Vertex(point=SphericalPoint(*point)) for point in nucleus['points']]
        self.nucleusedges = []
        self.nucleusfaces = []
        for faceindices in nucleus['faces']:
            nucleusfacevertices = [self.nucleusvertices[i] for i in faceindices]
            nucleusfaceedges = [HalfEdge(vertex) for vertex in nucleusfacevertices]
            self.nucleusfaces.append(Face(*nucleusfaceedges))
            self.nucleusedges.extend(nucleusfaceedges)
        HalfEdge.fusion(self.nucleusedges)

        self.maketiles()

        verticesbyid = {}
        self.faces = []
        self.edges = []
        for nucleusface in self.nucleusfaces:
            face6vertices = []
            edge = nucleusface.oneedge
            for _ in range(3):
                face6vertices.extend([edge.vertex, edge.opposite.next.next.vertex])
                edge = edge.next
            
            for sextuples in self.tiles:
                tilevertices = []
                for sextuple in sextuples:
                    vertex = Vertex(weights=sextuple, vertices=face6vertices)
                    tilevertices.append(verticesbyid.setdefault(vertex.ident, vertex))
                tileedges = [HalfEdge(vertex) for vertex in tilevertices]
                self.faces.append(Face(*tileedges))
                self.edges.extend(tileedges)
            #break
        HalfEdge.fusion(self.edges)
        self.vertices = list(verticesbyid.values())
        assert(len(self.faces) == len(self.nucleusfaces) * (self.m*self.m + self.n*self.n + self.m*self.n))

        for vertex in self.vertices:
            vertex.project()

    def openscad(self, R=100, color='yellow'):
        modulename = f'geode_{self.N}_{self.m}_{self.n}'
        print(f"function {modulename}_points() =")
        print(f"{[[R*vertex.point.x, R*vertex.point.y, R*vertex.point.z] for vertex in self.vertices]};")
        print()
        
        faces = []
        for face in self.faces:
            vertices = [face.oneedge.vertex]
            edge = face.oneedge.next
            while edge != face.oneedge:
                vertices.append(edge.vertex)
                edge = edge.next
            faces.append([self.vertices.index(vertex) for vertex in vertices])
        print(f"function {modulename}_faces() =")
        print(f"{faces};")
        print()

        print(f"module {modulename}(R={R}) {{")
        print(f" polyhedron(R/{R}*{modulename}_points(), {modulename}_faces());")
        print("};\n")

        print(f'color("{color}"){modulename}();')


    def maketiles(self):      
        m = self.m
        n = self.n
        grid = [[None]*(m+n+1) for _ in range(m+n+1)]
        for i in range(-n, m+1):
            jmin = max(-n, -n-i)
            jmax = min(m, m-i)
            for j in range(jmin, jmax+1):
                k = m-n-i-j
                alpha,beta,gamma = self.bc(i, j, k)
                alphaprime = betaprime = gammaprime = 0
                if alpha>0 and beta>0 and gamma>0:
                    location = +1 # inside
                elif alpha*beta*gamma == 0:
                    location = 0 # border
                else:
                    location = -1 # outside
                    if alpha<0:
                        alpha = 0
                        # j → -n - j
                        # k →  m - k
                        alphaprime,gamma,beta = self.bc(j+k, -n-j, m-k)
                    elif beta<0:
                        beta = 0
                        # i → -n - i 
                        # j →  m - j
                        gamma,betaprime,alpha = self.bc(-n-i, m-j, i+j)
                    elif gamma<0:
                        gamma = 0
                        # k → -n - k
                        # i →  m - i
                        beta,alpha,gammaprime = self.bc(m-i, k+i, -n-k)
                grid[i][j] = (location, (alpha,gammaprime,beta,alphaprime,gamma,betaprime))
        
        self.tiles = []
        for i in range(-n, m+1):
            jmin = max(-n, -n-i)
            jmax = min(m, m-i)
            for j in range(jmin, jmax+1):
                tile = self.newtile(grid, (i,j), (i-1,j), (i-1,j+1))
                if tile:
                    self.tiles.append(tile)
                tile = self.newtile(grid, (i,j), (i+1,j), (i+1,j-1))
                if tile:
                    self.tiles.append(tile)
        assert(len(self.tiles) == m*m + n*n + m*n)  

    def bc(self, i, j, k):
        # barycentric coordinates
        alpha  = self.m*self.n + self.m*j - self.n*k
        beta   = self.m*self.n + self.m*i - self.n*j  
        gamma  = self.m*self.n + self.m*k - self.n*i      
        return alpha,beta,gamma

    def newtile(self, grid, *points):
        for i,j in points:
            k = self.m-self.n-i-j
            if i < -self.n or i > self.m or j < -self.n or j > self.m or k < -self.n or k > self.m:
                return None
        locations,sextuples = zip(*(grid[i][j] for i,j in points))
        if sum(locations) < 0:
            return None
        if sum(locations) == 0 and \
           locations not in ((0,0,0), (1,0,-1), (0,-1,1), (-1,1,0)):
            return None
        return sextuples

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser("Geode")
    parser.add_argument("nucleus")
    parser.add_argument("m", type=int)
    parser.add_argument("n", type=int)
    args = parser.parse_args()
    geode = Geode(args.nucleus, args.m, args.n)
    geode.openscad()
    lengths = HalfEdge.lengths(geode.edges)
    lengths = [l / lengths[-1] for l in lengths]
    import itertools
    for k,g in itertools.groupby(lengths, key=lambda l:math.ceil(l*100)/100):
        print(f"{k:.2f} x {len(list(g))}", file=sys.stderr)
